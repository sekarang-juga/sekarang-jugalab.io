<!DOCTYPE html>
<html class="nojs" lang="en-US" dir="ltr">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">

<?php if ( $pagination === 1 ) { ?>
	<title><?php echo BLOGNAME; ?> ~ Online Shopping for Popular Electronics, Fashion, Home & Garden, Toys & Sports, Automobiles and More.</title>
	<meta name="description" content="<?php echo BLOGNAME; ?> ~ Online Shopping for Popular Electronics, Fashion, Home & Garden, Toys & Sports, Automobiles and More."/>
	<meta name="robots" content="NOARCHIVE, NOTRANSLATE"/>
<?php } else { ?>
	<title><?php echo "Page {$pagination}";?> <?php echo BLOGNAME; ?> ~ Online Shopping for Popular Electronics, Fashion, Home & Garden, Toys & Sports, Automobiles and More.</title>
	<meta name="description" content="<?php echo "Page {$pagination}";?> <?php echo BLOGNAME; ?> ~ Online Shopping for Popular Electronics, Fashion, Home & Garden, Toys & Sports, Automobiles and More."/>
	<meta name="robots" content="NOARCHIVE, NOTRANSLATE, FOLLOW, NOINDEX"/>
<?php } ?>



<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/sir-reynolds/ssg-assets@main/hugo/zen/all.css">

<?php echo WEBMASTER; ?>
<style>
@media (min-width:999px){.layout__sidebar-second{grid-template-areas:'head head' 'nav nav' 'top side2' 'main side2' 'bottom side2' 'foot foot';grid-template-columns:1fr}} .pagination a {padding: .5rem 1rem; margin: 0 .25rem; background: #eee; border-radius: .25rem; -webkit-border-radius: .25rem;}
</style>
</head>
<body class="list-page front">

<div class="page layout__page layout__sidebar-second">
<header class="header layout__header">
<a href="/" title="<?php echo BLOGNAME; ?>" rel="home" class="header__logo"><img src="https://i0.wp.com/themes.gohugo.io/theme/hugo-theme-zen/images/logo.png?quality=75" srcset="https://i0.wp.com/themes.gohugo.io/theme/hugo-theme-zen/images/logo.png?resize=192,192&quality=75 192w" alt="<?php echo BLOGNAME; ?>" class="header__logo-image" width="64" height="64"></a>
<h1 class="header__site-name">
<a href="/" title="Home" class="header__site-link" rel="home"><span><?php echo BLOGNAME; ?></span></a>
</h1>
<div class="region header__region">
</div>
</header>

<nav class="main-menu layout__navigation">
<h2 class="visually-hidden">Main menu</h2>
<ul class="navbar">
<li><a href="/" class="active" aria-current="page">Home</a></li>
</ul>
</nav>


<main class="main layout__main">
<article class="single-view">
<div class="content">

</div>
</article>


			<?php 
				foreach( $arr_per_pages as $paging_content ) { 
					$paging_productId 		= 	arr_get( $paging_content, 'productId' ); 
					$paging_productTitle 	= 	arr_get( $paging_content, 'productTitle' ); 
					$paging_salePrice 		= 	arr_get( $paging_content, 'salePrice' ); 
					$paging_productTitle	=	htmlentities( $paging_productTitle );
					$paging_permalink		=	"https://".BLOGNAME."/items/{$paging_productId}.html";
			?>
				<article class="section-post list-view">
					<header>
					<h2 class="title title-submitted"><a href="<?php echo $paging_permalink;?>"><?php echo $paging_productTitle;?></a></h2>
					<div class="submitted">
					<span class="author" itemprop="author"><?php echo BLOGNAME; ?></span>
					</div>
					</header>
					<div class="content">
					<p>Review about <?php echo $paging_productTitle;?>.</p>
					</div>
				</article>
			<?php } ?>
			
			<hr/>
			<div class="d-flex justify-content-center pagination" style="text-align:center;margin-top:25px;">
				<a href="<?php echo $prevUrl;?>">&larr; Prev</a>
				<a href="<?php echo $nextUrl;?>">Next &rarr;</a>
			</div>

</main>



<footer class="footer layout__footer">
<p>SSG Under @TheGreatSpammer</p>
</footer>

</div>


</body>
</html>
