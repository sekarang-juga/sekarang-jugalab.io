<?php

class Minifier
{
    /**
     * @var string
     */
    private static $X = "\x1A";

    /**
     * @var string
     */
    private static $SS = '"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'';

    /**
     * @var string
     */
    private static $CC = '\/\*[\s\S]*?\*\/';

    /**
     * @var string
     */
    private static $CH = '<\!--[\s\S]*?-->';

    /**
     * @param string $input
     * 
     * @return string
     */
    private static function minifyX( $input )
    {
        return str_replace( [ "\n", "\t", ' ' ], [ static::$X . '\n', static::$X . '\t', static::$X . '\s' ], $input );
    }

    /**
     * @param string $input
     * 
     * @return string
     */
    private static function minifyV( $input )
    {
        return str_replace( [ static::$X . '\n', static::$X . '\t', static::$X . '\s' ], [ "\n", "\t", ' ' ], $input );
    }

    /**
     * @param string $input
     * 
     * @return string
     */
    private static function preHtml( $input )
    {
        return preg_replace_callback( '#<\s*([^\/\s]+)\s*(?:>|(\s[^<>]+?)\s*>)#', function( $value ) {
            if ( isset( $value[2] ) ) {
                if ( stripos( $value[2], ' style=' ) !== false) {
                    $value[2] = preg_replace_callback( '#(style=)([\'"]?)(.*?)\2#i', function( $value ) {
                        return $value[1] . $value[2] . static::css( $value[3] ) . $value[2];
                    }, $value[2] );
                }
                
                return '<' . $value[1] . preg_replace( [ '#\s(checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped)(?:=([\'"]?)(?:true|\1)?\2)#i', '#\s*([^\s=]+?)(=(?:\S+|([\'"]?).*?\3)|$)#', '#\s+\/$#' ], [ ' $1', ' $1$2', '/' ], str_replace( "\n", ' ', $value[2] ) ) . '>';
            }
            
            return '<' . $value[1] . '>';
        }, $input );
    }

    /**
     * @param string $input
     * 
     * @return string
     */
    public static function html( $input ) {
        if ( ! $input = trim( $input ) ) {
            return $input;
        }
        
        $input = preg_replace( '#(<(?:img|input)(?:\s[^<>]*?)?\s*\/?>)\s+#i', '$1' . static::$X . '\s', $input );
        $input = preg_split( '#(' . static::$CH . '|<pre(?:>|\s[^<>]*?>)[\s\S]*?<\/pre>|<code(?:>|\s[^<>]*?>)[\s\S]*?<\/code>|<script(?:>|\s[^<>]*?>)[\s\S]*?<\/script>|<style(?:>|\s[^<>]*?>)[\s\S]*?<\/style>|<textarea(?:>|\s[^<>]*?>)[\s\S]*?<\/textarea>|<[^<>]+?>)#i', $input, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE );
        $output = "";
        
        foreach ( $input as $value ) {
            if ( $value !== ' ' && trim( $value ) === "" ) {
                continue;
            }
        
            if ( $value[0] === '<' && substr( $value, -1 ) === '>' ) {
                if ( $value[1] === '!' && strpos( $value, '<!--' ) === 0 ) {
                    if ( substr( $value, -12 ) !== '<![endif]-->' ) {
                        continue;
                    }
        
                    $output.= $value;
                } else {
                    $output.= static::minifyX( static::preHtml( $value ) );
                }
            } else {
                $value = str_replace( [ '&#10;', '&#xA;', '&#xa;' ], static::$X . '\n', $value );
                $value = str_replace( [ '&#32;', '&#x20;' ], static::$X . '\s', $value );
                $output.= preg_replace( '#\s+#', ' ', $value );
            }
        }
        
        $output = preg_replace( [ '#>([\n\r\t]\s*|\s{2,})<#', '#\s+(<\/[^\s]+?>)#' ], [ '><', '$1' ], $output );
        $output = static::minifyV( $output );
        
        return preg_replace( '#<(code|pre|script|style)(>|\s[^<>]*?>)\s*([\s\S]*?)\s*<\/\1>#i', '<$1$2$3</$1>', $output );
    }

    /**
     * @param string $input
     * 
     * @return string
     */
    private static function preCss( $input )
    {
        if ( stripos( $input, 'calc(' ) !== false ) {
            $input = preg_replace_callback( '#\b(calc\()\s*(.*?)\s*\)#i', function( $value ) {
                return $value[1] . preg_replace( '#\s+#', static::$X . '\s', $value[2] ) . ')';
            }, $input );
        }
        
        return preg_replace( [ '#(?<![,\{\}])\s+(\[|:\w)#', '#\]\s+#', '#\b\s+\(#', '#\)\s+\b#', '#\#([\da-f])\1([\da-f])\2([\da-f])\3\b#i', '#\s*([~!@*\(\)+=\{\}\[\]:;,>\/])\s*#', '#\b(?:0\.)?0([a-z]+\b|%)#i', '#\b0+\.(\d+)#', '#:(0\s+){0,3}0(?=[!,;\)\}]|$)#', '#\b(background(?:-position)?):(0|none)\b#i', '#\b(border(?:-radius)?|outline):none\b#i', '#(^|[\{\}])(?:[^\{\}]+)\{\}#', '#;+([;\}])#', '#\s+#' ], [ static::$X . '\s$1', ']' . static::$X . '\s', static::$X . '\s(', ')' . static::$X . '\s', '#$1$2$3', '$1', '0', '.$1', ':0', '$1:0 0', '$1:0', '$1', '$1', ' ' ], $input );
    }

    /**
     * @param string $input
     * 
     * @return string
     */
    public static function css( $input )
    {
        if ( ! $input = trim( $input ) ) {
            return $input;
        }
        
        $input = preg_replace( '#(' . static::$CC . ')\s+(' . static::$CC . ')#', '$1' . static::$X . '\s$2', $input );
        $input = preg_split( '#(' . static::$SS . '|' . static::$CC . ')#', $input, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE );
        $output = "";
        
        foreach ( $input as $value ) {
            if ( trim( $value ) === "" ) {
                continue;
            }
            
            if ( ( $value[0] === '"' && substr( $value, -1 ) === '"' ) || ( $value[0] === "'" && substr($value, -1) === "'" ) || ( strpos( $value, '/*' ) === 0 && substr( $value, -2 ) === '*/' ) ) {
                if ( $value[0] === '/' && strpos( $value, '/*!' ) !== 0 ) {
                    continue;
                }
                
                $output.= $value;
            } else {
                $output.= static::preCss( $value );
            }
        }
        
        $output = preg_replace( [ '#(' . static::$CC . ')|\b(url\()([\'"])([^\s]+?)\3(\))#i' ], [ '$1$2$4$5' ], $output );
        
        return static::minifyV( $output );
    }

    /**
     * @param string $input
     * 
     * @return string
     */
    public static function preJs( $input )
    {
        return preg_replace( [ '#\s*\/\/.*$#m', '#\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#', '#[;,]([\]\}])#', '#\btrue\b#', '#\bfalse\b#', '#\breturn\s+#' ], [ "", '$1', '$1', '!0', '!1', 'return ' ], $input );
    }

    /**
     * @param string $input
     * 
     * @return string
     */
    public static function js( $input )
    {
        if ( ! $input = trim( $input ) ) {
            return $input;
        }
        
        $input = preg_split( '#(' . static::$SS . '|' . static::$CC . '|\/[^\n]+?\/(?=[.,;]|[gimuy]|$))#', $input, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE );
        $output = "";
        
        foreach ( $input as $value ) {
            if ( trim( $value ) === "" ) {
                continue;
            }
            
            if ( ( $value[0] === '"' && substr( $value, -1 ) === '"' ) || ( $value[0] === "'" && substr( $value, -1 ) === "'" ) || ( $value[0] === '/' && substr( $value, -1 ) === '/' ) ) {
                if ( strpos( $value, '//' ) === 0 || ( strpos( $value, '/*' ) === 0 && strpos( value, '/*!' ) !== 0 && strpos( $value, '/*@cc_on' ) !== 0 ) ) {
                    continue;
                }
                
                $output.= $value;
            } else {
                $output.= static::preJs( $value );
            }
        }
        
        return preg_replace( [ '#(' . static::$CC . ')|([\{,])([\'])(\d+|[a-z_]\w*)\3(?=:)#i', '#([\w\)\]])\[([\'"])([a-z_]\w*)\2\]#i' ], [ '$1$2$4', '$1.$3' ], $output );
    }
}